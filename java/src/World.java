
public class World {
	private Cell[][] _plane;
	private int _xLimit;
	private int _yLimit;
	
	public World(int x, int y) {
		_xLimit = x;
		_yLimit = y;
		_plane = new Cell[x][y];
		
		populatePlane();
		loadTemplate();
	}
	
	
	public void populatePlane()
	{
		for (int i = 0; i<_xLimit; i++)
		{
			for (int j = 0; j<_yLimit; j++)
			{
				_plane[i][j] = new Cell(i, j, false, (byte)0);
			}
		}
	}

	
	private void loadTemplate()
	{
		int[][] template = Template.getTemplate();
		for (int i=0; i< template.length; i++)
		{
			_plane[template[i][0]][template[i][1]].setState(true);
		}
	}
	
	
	public void drawWorld()
	{
		for(int i=0; i<_yLimit; i++)
		{
			for(int j=0; j<_xLimit; j++)
			{
				System.out.print(_plane[j][i].getVisual());
			}
			System.out.print("|\n");
		}
		
		System.out.println("-------------------------------------------------------------");
	}
	
	
	public boolean tick()
	{
		drawWorld();
		boolean gameOver = false;
		nextGeneration();
		return gameOver;
	}
	
	private void nextGeneration()
	{
		Cell[][] oldPlane = getPlaneDeepCopy();
		//goes through all coordinates
		for (int currentX = 0; currentX < _xLimit; currentX++)
		{
			for (int currentY = 0; currentY < _yLimit; currentY++)
			{
				//checks for and counts live neighbors
				byte liveNeighbours = 0; //because it counts itself
				
				for(byte modX = -1; modX<=1; modX++)
				{
					for(byte modY = -1; modY<=1; modY++)
					{
						
						if(currentX+modX>=0 && currentY+modY>=0 && currentX+modX <_xLimit && currentY+modY<_yLimit)
						{
							if(oldPlane[currentX+modX][currentY+modY].isAlive())
							{
								liveNeighbours++;
							}
						}
					}
				}
				
				if(oldPlane[currentX][currentY].isAlive())
				{
					liveNeighbours--;
				}
				
				_plane[currentX][currentY].setNeighbourCount(liveNeighbours);
			}
		}
	}
	
	private Cell[][] getPlaneDeepCopy()
	{
		Cell[][] copy = new Cell[_xLimit][_yLimit];
		for(int y=0; y<_yLimit; y++)
		{
			for(int x=0; x<_xLimit; x++)
			{
				copy[x][y] = new Cell(x, y, _plane[x][y].isAlive(), _plane[x][y].getNeighbours());
			}
		}
		return copy;
	}
}
