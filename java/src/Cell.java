
public class Cell {
	private byte _neighbours;
	private boolean _alive;
	
	public Cell(int x, int y, boolean alive, byte neighbours) {
		_neighbours = neighbours;
		_alive = alive;
	}
	
	public boolean isAlive()
	{
		return _alive;
	}
	
	public void setState(boolean alive)
	{
		_alive = alive;
	}
	
	public void setNeighbourCount(byte neighbours)
	{
		_neighbours = neighbours;
		reproduction();
	}
	
	private void reproduction()
	{
		if(_neighbours<2 || _neighbours>3)
		{
			_alive = false;
		}
		
		else if(_neighbours==3)
		{
			_alive = true;
		}
	}
	
	public char getVisual()
	{
		if(_alive)
		{
			return '■';
		}
		else
		{
			return '+';
		}
		//return (char)(_neighbours+48);//temp
	}
	
	public byte getNeighbours()
	{
		return _neighbours;
	}
}
